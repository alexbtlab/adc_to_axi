vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xpm
vlib riviera/xlconstant_v1_1_6

vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm
vmap xlconstant_v1_1_6 riviera/xlconstant_v1_1_6

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ipshared/c923" "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ip/design_1_ADC_GET_DATA_0_0/src/clk_wiz_1" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ipshared/c923" "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ip/design_1_ADC_GET_DATA_0_0/src/clk_wiz_1" \
"../../../bd/design_1/ipshared/e0f2/hdl/ADC_to_AXI_v1_0_M00_AXIS.v" \
"../../../bd/design_1/ipshared/e0f2/hdl/ADC_to_AXI_v1_0.v" \
"../../../bd/design_1/ip/design_1_ADC_to_AXI_0_0/sim/design_1_ADC_to_AXI_0_0.v" \
"../../../bd/design_1/ipshared/82b5/hdl/counter_v1_0.v" \
"../../../bd/design_1/ip/design_1_counter_0_0/sim/design_1_counter_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0.v" \

vlog -work xlconstant_v1_1_6  -v2k5 "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ipshared/c923" "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ip/design_1_ADC_GET_DATA_0_0/src/clk_wiz_1" \
"../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ipshared/66e7/hdl/xlconstant_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ipshared/c923" "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ip/design_1_ADC_GET_DATA_0_0/src/clk_wiz_1" \
"../../../bd/design_1/ip/design_1_xlconstant_0_0/sim/design_1_xlconstant_0_0.v" \
"../../../bd/design_1/ip/design_1_ADC_GET_DATA_0_0/src/clk_wiz_1/clk_wiz_1_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_ADC_GET_DATA_0_0/src/clk_wiz_1/clk_wiz_1.v" \

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ipshared/c923" "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ip/design_1_ADC_GET_DATA_0_0/src/clk_wiz_1" \
"../../../bd/design_1/ipshared/d258/hdl/adc_get_data.sv" \
"../../../bd/design_1/ip/design_1_ADC_GET_DATA_0_0/sim/design_1_ADC_GET_DATA_0_0.sv" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ipshared/c923" "+incdir+../../../../ADC_to_AXI.srcs/sources_1/bd/design_1/ip/design_1_ADC_GET_DATA_0_0/src/clk_wiz_1" \
"../../../bd/design_1/sim/design_1.v" \

vlog -work xil_defaultlib \
"glbl.v"

