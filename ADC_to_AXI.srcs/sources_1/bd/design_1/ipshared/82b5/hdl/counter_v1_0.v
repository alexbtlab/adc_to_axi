module counter(
        input wire clk_100MHz,
		input wire clk_5MHz_count,
		input wire reset,
		input wire start,
		input wire [15:0] FrameSize,
		output wire ready,
		output wire [31:0] count
);

reg [15:0] cnt_5MHz = 0;

assign ready = clk_5MHz_count;

reg start_reg = 0;
reg flag_count_start = 0;
assign  count = cnt_5MHz;

always @(posedge start) begin
    start_reg <= 1;
    //flag_count_start = 1;     
end
always @(posedge clk_5MHz_count) begin

    if(!reset) begin
        //cnt_5MHz <= 32'h0000;
    end
    else begin
      if(flag_count_start) begin
        if(cnt_5MHz == (FrameSize+1)) begin
            flag_count_start = 0;
            cnt_5MHz <= 32'h0000;
        end
        else begin
            cnt_5MHz <= cnt_5MHz + 1;
        end
      end
      else begin 
        if(start_reg) 
            flag_count_start = 1;
      end
    end         
end
endmodule