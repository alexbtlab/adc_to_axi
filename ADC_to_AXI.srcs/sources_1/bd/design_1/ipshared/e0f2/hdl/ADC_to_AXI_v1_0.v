
`timescale 1 ns / 1 ps

	module ADC_to_AXI_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
		// Users to add ports here
        input wire [15:0]  frameSize,
        input wire [31:0]  data,
        input wire         start,
        input wire         ready_clk,
		// User ports ends
		output wire  ready_clk_inv,
		// Do not modify the ports beyond this line
        output wire  clk_100_axi_out,
        
        output wire  [31:0] ila_data_out_ram,
        output wire  [15:0] ila_wordCounter,
        output wire  [15:0] ila_adr,
        
        output wire  ila_frd,
        output wire  ila_fwr,

        output wire  ila_fstop,
        output wire  ila_fstart,
        
		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready
	);
	
	wire [31:0] data_to_transmit = 0;
	assign ila_data_out_ram = data_to_transmit;
	
	reg flag_read = 0;
	assign ila_frd = flag_read;
	
	wire i_write;
	reg flag_write = 0;
    assign i_write = flag_write;
	assign ila_fwr = flag_write;
	
	wire [15:0] i_addr;
	reg [15:0] adr = 0;
    assign i_addr = adr;
    assign ila_adr = adr;
    
    reg flag_stop = 0;
    reg flag_start = 0;
    assign ila_fstop = flag_stop;
    assign ila_fstart = flag_start;
	
	reg [31:0] o_data = 0;
	
	//assign data_to_transmit = (flag_read) ? o_data : 0;
	
	wire clk_100_axi;
	assign clk_100_axi     = (flag_read) ? m00_axis_aclk : 0;
	assign clk_100_axi_out = clk_100_axi;
	
	wire last_w;
	assign m00_axis_tlast = last_w;//(flag_read & (adr == (frameSize+1))) ? last_w : 0;
	
	wire valid_w;
	assign m00_axis_tvalid = valid_w;//(flag_read & (adr > 0) & (adr >= (frameSize+1))) ? valid_w : 0;
	
// Instantiation of Axi Bus Interface M00_AXIS
	ADC_to_AXI_v1_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT(C_M00_AXIS_START_COUNT)
	) ADC_to_AXI_v1_0_M00_AXIS_inst (
        .FrameSize(frameSize),
		.En(1),		
		.en_AXIclk(flag_read), 
		.main_clk_100MHz(m00_axis_aclk),
		.M_AXIS_ACLK(clk_100_axi),
		.M_AXIS_ARESETN(m00_axis_aresetn),
		.ila_wordCounter(ila_wordCounter),
		.M_AXIS_TVALID(valid_w),
		.data_to_transmit(o_data),
		.M_AXIS_TDATA(m00_axis_tdata),
		.M_AXIS_TSTRB(m00_axis_tstrb),
		.M_AXIS_TLAST(last_w),
		.M_AXIS_TREADY(1)
	);

	// Add user logic here
    assign ready_clk_inv = ~ready_clk;
	// User logic ends
///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////
    wire i_clk;
    
    
   



        

    wire [31:0] i_data;
    assign i_data = (flag_write) ? data : 0; 

    reg [31:0] memory_array [0:8192-1]; 
    
    wire clk_5_100;
    assign clk_5_100 = (flag_read) ? ~m00_axis_aclk : ready_clk_inv;
    
    wire clk_5_100_a;
    assign clk_5_100_a = (flag_read) ? m00_axis_aclk : ready_clk;
    
    always @ (posedge clk_5_100)
    begin
        if(flag_write) begin
            memory_array[i_addr] <= i_data;
        end
        else begin
            o_data <= memory_array[i_addr];
        end     
    end
////////////////////////////////////////////////////////////////////////////////////////////////////////



always @(posedge m00_axis_aclk) begin
    if(start & !flag_read & !flag_stop) begin
        flag_write = 1; 
    //    flag_start = 1;
    end
    if(flag_stop) begin
        //flag_stop <= 0;
        flag_write <= 1; 
    end
    if(adr == (frameSize+1)) begin
        flag_write <= 0; 
    end    
    
        
end
always @(posedge clk_5_100_a) begin

    if(!m00_axis_aresetn) begin
        //cnt_5MHz <= 32'h0000;
    end
    else begin
      if(flag_start) begin
        if(adr == (frameSize+1)) begin
            adr <= 32'h0000;
            if(flag_read == 1) begin
                flag_read = 0;
                flag_start = 0;
                flag_stop = 1;
            end
            else begin
                flag_read = 1;
            end
        end
        else begin
            adr <= adr + 1;
        end
        
      end
      else begin
        if(start & !flag_read & !flag_stop) begin 
            flag_start = 1;
        end
      end
      
      if(flag_stop) begin
        flag_stop <= 0;
        //flag_write <= 1; 
      end
      
    end         
end
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	endmodule
