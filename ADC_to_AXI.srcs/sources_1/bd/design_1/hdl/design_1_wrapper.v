//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Thu Jun 18 22:47:18 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (clk_100,
    m00_axis_aresetn_0,
    start_0);
  input clk_100;
  input m00_axis_aresetn_0;
  input start_0;

  wire clk_100;
  wire m00_axis_aresetn_0;
  wire start_0;

  design_1 design_1_i
       (.clk_100(clk_100),
        .m00_axis_aresetn_0(m00_axis_aresetn_0),
        .start_0(start_0));
endmodule
