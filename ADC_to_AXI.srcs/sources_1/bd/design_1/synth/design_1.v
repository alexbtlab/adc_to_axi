//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Thu Jun 18 22:47:18 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=6,numReposBlks=6,numNonXlnxBlks=2,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (clk_100,
    m00_axis_aresetn_0,
    start_0);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_100 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_100, ASSOCIATED_RESET m00_axis_aresetn_0, CLK_DOMAIN design_1_clk_100, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input clk_100;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.M00_AXIS_ARESETN_0 RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.M00_AXIS_ARESETN_0, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input m00_axis_aresetn_0;
  input start_0;

  wire clk_in1_0_1;
  wire clk_wiz_0_clk_out1;
  wire clk_wiz_1_clk_out1;
  wire [31:0]counter_0_count;
  wire counter_0_ready;
  wire m00_axis_aresetn_0_1;
  wire start_0_1;
  wire [15:0]xlconstant_0_dout;

  assign clk_in1_0_1 = clk_100;
  assign m00_axis_aresetn_0_1 = m00_axis_aresetn_0;
  assign start_0_1 = start_0;
  design_1_ADC_GET_DATA_0_0 ADC_GET_DATA_0
       (.adc_dax_n(1'b0),
        .adc_dax_p(1'b0),
        .adc_dbx_n(1'b0),
        .adc_dbx_p(1'b0),
        .adc_dcox_n(1'b0),
        .adc_dcox_p(1'b0),
        .fpga_clkx_n(1'b0),
        .fpga_clkx_p(1'b0),
        .sim_adc_da(clk_in1_0_1),
        .sim_adc_db(clk_in1_0_1),
        .sim_adc_dco(clk_in1_0_1),
        .sim_fpga_clk(clk_in1_0_1));
  design_1_ADC_to_AXI_0_0 ADC_to_AXI_0
       (.data(counter_0_count),
        .frameSize(xlconstant_0_dout),
        .m00_axis_aclk(clk_in1_0_1),
        .m00_axis_aresetn(m00_axis_aresetn_0_1),
        .m00_axis_tready(1'b1),
        .ready_clk(counter_0_ready),
        .start(start_0_1));
  design_1_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(clk_in1_0_1),
        .clk_out1(clk_wiz_0_clk_out1));
  design_1_clk_wiz_1_0 clk_wiz_1
       (.clk_in1(clk_wiz_0_clk_out1),
        .clk_out1(clk_wiz_1_clk_out1));
  design_1_counter_0_0 counter_0
       (.FrameSize(xlconstant_0_dout),
        .clk_100MHz(clk_in1_0_1),
        .clk_5MHz_count(clk_wiz_1_clk_out1),
        .count(counter_0_count),
        .ready(counter_0_ready),
        .reset(m00_axis_aresetn_0_1),
        .start(start_0_1));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule
