`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 15.06.2020 21:37:03
// Design Name: 
// Module Name: tb_ADC_to_AXI
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_ADC_to_AXI(

    );
    
    
  reg clk_100;
  reg m00_axis_aresetn_0;
  reg start_0;

initial begin
    clk_100 = 0;
    forever #5 clk_100 = ~clk_100;
end
initial begin
    m00_axis_aresetn_0 = 0;
    #22000 m00_axis_aresetn_0 = 1;
end
initial begin
    start_0 = 0;
    #24000 start_0 = 1;
    #24100 start_0 = 0;
    
    #60000 start_0 = 1;
    #60100 start_0 = 0;
end
  design_1_wrapper DUT
       (.clk_100(clk_100),
        .m00_axis_aresetn_0(m00_axis_aresetn_0),
        .start_0(start_0));   
    
    
endmodule
